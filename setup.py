from setuptools import setup

setup(
  name = 'solve_dde',
  version='1.0',
  author='Jerome Jansen',
  author_email='jrme.jansen@gmail.com',
  packages=['solve_dde'], #, 'pyTG.test'],
  # scripts=['scripts/growthPyDonadoni_1D_fi', 'scripts/growthPyFOAM_Donadoni',
      # 'scripts/growthPyFOAM_JansenBessel_2D_fi', 'scripts/growthPyJansenBessel_1D_fi',
      # 'scripts/growthPyFOAM_JansenBessel_2D_noEq_fi',
      # 'scripts/growthPyJansenBessel_NoHemo_1D_fi','scripts/growthPyJansenBessel_SC_fi',
      # 'scripts/arrangment_data', 'scripts/postProcessBetween',
      # 'scripts/generateLayers_2D', 'scripts/generateLayers_3D'],
        #,'scripts/','scripts/','scripts/'],
  # url='http://pypi.python.org/pypi/PackageName/',
  url='https://gitlab.com/jrme.jansen/solve_dde',
  license='LICENSE',
  description='Package for solving constant delay differential equations.',
  long_description=open('README.md').read(),
  long_description_content_type='text/markdown',
  install_requires=[
      "numpy==1.17.4",
      "scipy==1.6.0",
      "matplotlib==3.1.2",
  ],
)
