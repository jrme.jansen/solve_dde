from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np
from jitcdde import jitcdde
from jitcdde import y as y_jit
from jitcdde import t as t_jit


r = 0.5
mu = r / 10.0
c = np.sqrt(2)**-1

def fun(t,y,Z):
    if t <= 1 - c:
        f = -r * y[0] * 0.4 * (1 - t)
    elif t <= 1:
        f = -r * y[0] * (0.4 * (1 - t) + 10.0 - np.exp(mu) * y[0])
    elif t <= 2 - c:
        f = -r * y[0] * (10. - np.exp(mu) * y[0])
    else:
        f = -r * np.exp(mu) * y[0] * (Z[:,0] - y[0])
    return [f]

tau = 1.0
y0 = [10.0]
jumps = [1.0 - c, 1.0, 2.0 - c]
t0 = 0.0
tf = 10.0
atol = 1e-10
rtol = 1e-5
tspan = [t0, tf]
delays = [tau]

st = [i for i in range(4)]
err23_jumps = []
err45_jumps = []
err23_noJumps = []
err45_noJumps = []
ref = 0.06302089869
for i in st:
    print('track %s stages' % i)
    sol_j23 = solve_dde(fun, tspan, delays, y0, y0, tracked_stages=i,
            method='RK23', jumps=jumps, atol=atol, rtol=rtol)
    y_j23 = sol_j23.y[0,:]
    err23_jumps.append(np.abs(y_j23[-1]-ref)/ref)

    sol_j45 = solve_dde(fun, tspan, delays, y0, y0, tracked_stages=i,
            method='RK45', jumps=jumps, atol=atol, rtol=rtol)
    y_j45 = sol_j45.y[0,:]
    err45_jumps.append(np.abs(y_j45[-1]-ref)/ref)

    sol23 = solve_dde(fun, tspan, delays, y0, y0, tracked_stages=i,
            method='RK23', atol=atol, rtol=rtol)
    y23 = sol23.y[0,:]
    err23_noJumps.append(np.abs(y23[-1]-ref)/ref)
    sol45 = solve_dde(fun, tspan, delays, y0, y0, tracked_stages=i,
            method='RK45', atol=atol, rtol=rtol)
    y45 = sol45.y[0,:]
    err45_noJumps.append(np.abs(y45[-1]-ref)/ref)

    print('\n')


plt.figure()
plt.plot(st, err45_jumps, 'o', label='jumps')
plt.plot(st, err45_noJumps, 'o', label='no jumps')
plt.xlabel('nbr of tracking stage')
plt.ylabel(r'$\varepsilon = (y^{ref}(10)-y(10))/y^{ref}(10)$')
plt.title('RK45')
plt.legend()
plt.figure()
plt.plot(st, err23_jumps, 'o', label='jumps')
plt.plot(st, err23_noJumps, 'o', label='no jumps')
plt.xlabel('nbr of tracking stage')
plt.ylabel(r'$\varepsilon = (y^{ref}(10)-y(10))/y^{ref}(10)$')
plt.title('RK23')
plt.legend()
plt.show()


