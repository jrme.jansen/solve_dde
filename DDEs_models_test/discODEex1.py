from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np


def fun(t,y,Z):
    if t <= 40.33:
        return [0.0]
    else:
        return [100]

y0 = 40.33

jumps = [40.33]
tf = 100.0
tspan = [0.0, tf]

delays = []

rtol = 1e-5
atol = 1e-10
sol23 = solve_dde(fun, tspan, delays, [y0], [y0],
                    method='RK23', atol=atol, rtol=rtol)
sol23_j = solve_dde(fun, tspan, delays, [y0], [y0], #tracked_stages=0,
                    method='RK23', jumps=jumps, atol=atol, rtol=rtol)

print('nfev of f without jumps option : %s' % (sol23.nfev))
print('nfev of f wit     jumps option : %s' % (sol23_j.nfev))
print('nfaild without jumps option : %s' % (sol23.nfailed))
print('nfaild with    jumps option : %s' % (sol23_j.nfailed))

def anaf(t):
    if t <= 40.33:
        return y0
    else:
        return (t - 40.33) * 100. + y0

t_j = sol23_j.t
y_j = sol23_j.y[0,:]
t_ = sol23.t
y_ = sol23.y[0,:]

ana_j = np.zeros(y_j.shape)
ana_ = np.zeros(y_.shape)

for i in range(len(t_)):
    ana_[i] = anaf(t_[i])
for i in range(len(t_j)):
    ana_j[i] = anaf(t_j[i])

err_j = np.abs(ana_j-y_j)/ana_j
err_ = np.abs(ana_-y_)/ana_

plt.figure()
plt.plot(t_, err_, 'o-', label='with jump')
plt.plot(t_j, err_j, 'o-', label="without jump")
plt.yscale('log')
plt.legend()
plt.xlabel(r'$t$')
plt.ylabel(r'$\varepsilon$')
plt.savefig('figures/discODEex1/error')

plt.figure()
plt.plot(t_, ana_, 'o-', label='ana')
plt.plot(sol23.t, sol23.y[0,:], 'o-', label ='without jump')
plt.plot(sol23_j.t, sol23_j.y[0,:], 'o-', label='with jump')
plt.xlabel("t")
plt.ylabel("y")
plt.legend()
plt.savefig('figures/discODEex1/y')

plt.figure()
plt.plot(sol23.t[0:-1], np.diff(sol23.t), 'o-', label='dt')
plt.plot(sol23_j.t[0:-1], np.diff(sol23_j.t), 'o-', label='dt jump')
plt.yscale('log')
plt.xlabel("t")
plt.ylabel("dt")
plt.legend()
plt.savefig('figures/discODEex1/dt')


plt.show()

