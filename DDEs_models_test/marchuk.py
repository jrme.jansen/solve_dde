from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import CubicHermiteSpline

"""
The Marchuk immunology model (Exercise 7 from Shampine 2000, Solving Delay 
Differential Equations with dde23)

Tested features:
    -
    -??  
    -??  

Comparison with??? 
"""

h1 = 2.0
h2 = 0.8
h3 = 1e4
h4 = 0.17
h5 = 0.5
h7 = 0.12
h8 = 8

h6 = [10, 300]


tau = 0.5

t0 = 0.0
tf = 60.0

jumps = [-1e-6]

def h(t):
    return [max(0.0,t + 1e-6),
            1,
            1,
            0]

y00 = h(t0)
atol = 1e-8
rtol = 1e-5
tspan = [t0, tf]
delays = [tau]

def ev(t, y, Z, h1, h2, h3, h4, h5, h6_i, h7, h8):

    V, C, F, m = y
    return m - 0.1
ev.terminal = True
ev.direction = 0

def fun(t, y, Z, h1, h2, h3, h4, h5, h6_i, h7, h8):

    V, C, F, m = y
    V_tau, C_tau, F_tau, m_tau = Z

    def eps(m):
        if m <= 0.1:
            return 1.0
        elif 0.1 <= m and m <=1.0:
            return (1. - m) * 10./9.
        else:
            print('problem cas non renseigne')

    return [(h1 - h2 * F) * V,
            eps(m) * h3 * F_tau * V_tau - h5*(C-1.0),
            h4 * (C - F) - h8 * F * V,
            h6_i * V - h7 * m]


sol = []
sol45 = []
for h6_i in h6:
    print('h6 = ', h6_i)
    args = (h1, h2, h3, h4, h5, h6_i, h7, h8)

    print('RK23')
    sol_i = solve_dde(fun, tspan, delays, y00, h, args=args,
                events=ev,
                method='RK23', jumps=jumps, atol=atol, rtol=rtol)
    while(sol_i.t[-1]<tf):
        print('restart at ',sol_i.t[-1])
        y0 = sol_i.y[:,-1]
        tspan = [sol_i.t[-1],tf]
        j = [sol_i.t[-1]]
        sol_i = solve_dde(fun, tspan, delays, y0, sol_i, args=args,
                events=ev,
                method='RK23', jumps=j, atol=atol, rtol=rtol)

    print('RK45')
    tspan = [t0, tf]
    sol45_i = solve_dde(fun, tspan, delays, y00, h, args=args,
                events=ev,
                method='RK45', jumps=jumps, atol=atol, rtol=rtol)
    while(sol45_i.t[-1]<tf):
        print('restart at ',sol45_i.t[-1])
        y0 = sol45_i.y[:,-1]
        tspan = [sol45_i.t[-1],tf]
        j = [sol_i.t[-1]]
        sol45_i = solve_dde(fun, tspan, delays, y0, sol45_i, args=args,
                events=ev,
                method='RK45', jumps=j, atol=atol, rtol=rtol)
    sol.append(sol_i)
    sol45.append(sol45_i)

# sol matlab
import scipy.io as spio
t_mat = []
y_mat = []
yp_mat = []
for h_i in h6:
    path_matlab = 'data_dde23/marchuk_h6_%s_dde23.mat' % int(h_i)
    mat = spio.loadmat(path_matlab, squeeze_me=True)
    t_mat.append(mat['t'])
    y_mat.append(mat['y'])
    yp_mat.append(mat['yp'])

err_RK23_dde23 = []
for i in range(len(sol)):
    err_ = []
    for k in range(len(y00)):
        eps = 1e-5
        mask = np.abs(sol[i].y[k,:]) > eps
        p_dev = CubicHermiteSpline(sol[i].t, sol[i].y[k,:],sol[i].yp[k,:])
        y_dev_mat = p_dev(t_mat[i])
        mask = np.abs(y_dev_mat) > eps
        err_.append([t_mat[i][mask], np.abs(y_dev_mat[mask] - y_mat[i][k,mask]) / y_mat[i][k,mask]])
    err_RK23_dde23.append(err_)

f90 = np.loadtxt("data_dde_solver_fortran/marchukF90")
t_f90 = f90[:,0]
y_f90 = f90[:,1:].T

c = ['--', '-.']

for i in range(len(sol)):
    plt.figure()
    plt.plot(sol[i].t, sol[i].y[0,:] * 1e4, c[i], label='V * 1e4')
    plt.plot(sol[i].t, sol[i].y[1,:] * 0.5, c[i], label='C / 2')
    plt.plot(sol[i].t, sol[i].y[2,:], c[i], label='F')
    plt.plot(sol[i].t, sol[i].y[3,:] * 10, c[i], label='m * 10')
    plt.xlabel(r'$t$')
    plt.xlabel(r'')

    plt.plot(sol45[i].t, sol45[i].y[0,:] * 1e4, '*', label='V * 1e4')
    plt.plot(sol45[i].t, sol45[i].y[1,:] * 0.5, '*', label='C / 2')
    plt.plot(sol45[i].t, sol45[i].y[2,:], '*', label='F')
    plt.plot(sol45[i].t, sol45[i].y[3,:] * 10, '*', label='m * 10')

    plt.plot(t_mat[i], y_mat[i][0,:] * 1e4,  label='V * 1e4 mat')
    plt.plot(t_mat[i], y_mat[i][1,:] * 0.5, label='C / 2 mat')
    plt.plot(t_mat[i], y_mat[i][2,:],  label='F mat')
    plt.plot(t_mat[i], y_mat[i][3,:] * 10, label='m * 10 mat')

    if i==1:
        plt.plot(t_f90, y_f90[0,:] * 1e4,  label='f90')
        plt.plot(t_f90, y_f90[1,:] * 0.5, label='f90')
        plt.plot(t_f90, y_f90[2,:],  label='f90')
        plt.plot(t_f90, y_f90[3,:] * 10, label='f90')
        
    plt.legend()
    plt.savefig('figures/marchuk/y_h6_%s_events' % h6[i])

plt.figure()
plt.plot(sol[1].t, sol[1].y[3,:] * 10, c[1], label='RK23')
plt.plot(sol45[1].t, sol45[1].y[3,:] * 10, '*', label='RK45')
plt.plot(t_f90, y_f90[3,:] * 10, label='f90')
plt.plot(t_mat[1], y_mat[1][3,:] * 10, label='mat')
plt.legend()
plt.title('m*10 h6=300')
plt.savefig('figures/marchuk/m_events_h6_300')

plt.figure()
plt.plot(sol[0].t, sol[0].y[3,:] * 10, c[1], label='RK23')
plt.plot(sol45[0].t, sol45[0].y[3,:] * 10, '*', label='RK45')
# plt.plot(t_f90, y_f90[3,:] * 10, label='f90')
# plt.plot(t_mat[1], y_mat[1][3,:] * 10, label='mat')
plt.legend()
plt.title('m*10 h6=10')
plt.savefig('figures/marchuk/m_events_h6_10')

for i in range(len(sol)):
    plt.figure()
    plt.plot(sol[i].t[:-1],np.diff(sol[i].t),'-o',label='dt scipy-dev')
    plt.plot(t_mat[i][:-1],np.diff(t_mat[i]),'-o',label='dt matlab solver')
    if i==1:
        plt.plot(t_f90[:-1],np.diff(t_f90),'-o',label='dt f90')
    plt.legend()
    plt.ylabel(r'$\Delta t$')
    plt.xlabel(r'$t$')
    plt.savefig('figures/marchuk/dt_h6_%s' % h6[i])

for i in range(len(sol)):
    plt.figure()
    for k in range(len(y00)):
        plt.plot(err_RK23_dde23[i][k][0], err_RK23_dde23[i][k][1], label='err compo %s' % k)
    plt.legend()
    plt.xlabel(r'$t$')
    plt.title('h6= %s ' % h6[i])
    plt.ylabel(r'$\varepsilon$')
    plt.savefig('figures/marchuk/error_h6_%s' % h6[i])


plt.show()
