from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np
from sympy import *
from scipy.interpolate import CubicHermiteSpline

def fun(t,y,Z):
    y_tau1 = Z[:,0]
    y_tau2 = Z[:,1]
    return [-y_tau1 - y_tau2]

tau1 = 1.0
tau2 = 1./3.

t0 = 0.0
tf = 10.0
atol = 1e-10
rtol = 1e-5
tspan = [t0, tf]
delays = [tau1, tau2] #, tau3]

y0 = [1.5]
def h(t):
    return [1.0]

sol0 = solve_dde(fun, tspan, delays, y0, h, method='RK23', dense_output=True,
        atol=atol, rtol=rtol)
t = sol0.t
y = sol0.y[0,:]
yp = sol0.yp[0,:]

t_n = np.append(np.array([t0]), np.linspace(t0,tf,200))
y_interp = sol0.sol(t_n)[0,:]
print('\n restart \n')

t01 = 6.0
tspan = [t01, tf]
y0 = sol0.sol(t01)
sol1 = solve_dde(fun, tspan, delays, y0, sol0, method='RK23', atol=atol, rtol=rtol)
t_r = sol1.t
y_r = sol1.y[0,:]
yp_r = sol1.yp[0,:]


print('error y(10) restart vs not restart %s' % np.abs(np.abs(y_r[-1]-y[-1])/y[-1]))

plt.figure()
plt.plot(t, y, label='solve_dde y')
plt.plot(t_n, y_interp, label='interp y')
plt.plot(t, y,'s',markerfacecolor='none', label='solve_dde y')
plt.plot(t, yp, label='yp')
plt.xlabel(r'$t$')
plt.ylabel(r'$y(t)$')
plt.legend()

plt.figure()
plt.plot(t, y, label='y')
plt.plot(t_r, y_r, 'o', label='y_r')
plt.xlabel(r'$t$')
plt.ylabel(r'$y(t)$')
plt.legend()
plt.show()

