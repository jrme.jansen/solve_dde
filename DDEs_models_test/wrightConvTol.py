import time
from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import CubicHermiteSpline

t0 = 0.0
tf = 20
tspan = [t0, tf]
tau = 1
delays = [tau]

def h(t):
    return [t]

y0 = h(t0)


atol = [1e-9, 1e-10, 1e-11]
rtol = [1e-6, 1e-7, 1e-8]

sol_err = []
def fun(t, y, Z, lam, aa, bb):
    # print('Z', Z[:,0], 'y', y)
    return [-lam * Z[:,0] * (1.0 + y[0])]

for i in range(len(rtol)):
    args = (3.0, rtol[i], atol[i])
    print('l, rtol, atol ', args)
    rt = rtol[i]
    at = atol[i]
    t1 = time.time()
    sol23 = solve_dde(fun, tspan, delays, y0, h, method='RK23',
                      args=args,
                      atol=at, rtol=rt)
    t2 = time.time()
    print('tCPU ', t2-t1)
    sol_err.append(sol23)


yref = 4.67143
for i in range(len(rtol)):
    err = np.abs(sol_err[i].y[0,-1] - yref) / yref 
    # err_mat = np.abs(sol_mat[i].y[0,-1] - yref) / yref
    print('lambda = ', 3.0)
    print('nfev %s nfailed %s nOverlap %s' % (sol_err[i].nfev,
                                                sol_err[i].nfailed,
                                                sol_err[i].nOverlap))
    print('tf = ', sol_err[i].t[-1])
    print('scipy : y(tf)', sol_err[i].y[0, -1], 'ref', yref, 'err', err)
    # print('dde23 : y(tf)', sol_mat[i].y[0, -1], 'ref', yref, 'err_mat', err_mat)

# path = 'data_dde23/suitcase_dde23.mat'
# import scipy.io as spio
# mat = spio.loadmat(path, squeeze_me=True)
# t_mat = mat['t']
# y_mat = mat['y'][0,:]
# yp_mat = mat['y'][1,:]
plt.figure()
for i in range(len(rtol)):
    plt.plot(sol_err[i].t, sol_err[i].y[0,:], label=r'rtol=%s atol=%s' % (rtol[i], atol[i]))
    # plt.plot(t_mat, y_mat,'-', label=r'dde23 $\theta(t)$')
    plt.legend()
    # plt.savefig('figures/suitecase/t_y_yp')
plt.show()
