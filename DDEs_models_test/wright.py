import time
from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import CubicHermiteSpline
from jitcdde import jitcdde
from jitcdde import y as y_jit
from jitcdde import t as t_jit

"""
The `Wright` model (Example 4.2 from Oberle et al 1981
Numerical Treatment of Delay Differential Equations by Hermite Interpolation)

"The numerical sensitivity of the problem increases with increasing values of 
the parameter 2 due to the periodic peaks in the solution. Hence one might 
expect a loss of accuracy."

Tested features:
    - loss of accuracy as l increase

Comparison with dde23, jitcdde and ref value from Oberle.
"""

t0 = 0.0
tf = 20
tspan = [t0, tf]
tau = 1
delays = [tau]

def h(t):
    return [t]

y0 = h(t0)

atol = [1e-6, 1e-6, 1e-8, 1e-10]
rtol = [1e-3, 1e-3, 1e-5, 1e-6]

l = [1.5, 2.0, 2.5, 3.0]

l3ref = .467143e1
sol = []
sol45 = []
sol_jit = []

def fun(t, y, Z, lam,):
    # print('Z', Z[:,0], 'y', y)
    return [-lam * Z[:,0] * (1.0 + y[0])]

for i, li in enumerate(l):
    args = (li,)
    print(args)
    rt = rtol[i]
    at = atol[i]
    t1 = time.time()
    sol23 = solve_dde(fun, tspan, delays, y0, h, method='RK23',
                      args=args,
                      atol=at, rtol=rt)
    sol4 = solve_dde(fun, tspan, delays, y0, h, method='RK45',
                      args=args,
                      atol=at, rtol=rt)
    t2 = time.time()
    print('tCPU ', t2-t1)
    print('nfev %s nfailed %s nOverlap %s' % (sol23.nfev,
                                                sol23.nfailed,
                                                sol23.nOverlap))

    # jit cdde
    f = [ -li * y_jit(0,t_jit-tau) * (1.0 + y_jit(0))]
    DDE = jitcdde(f)
    print('at', at, 'rt', rt)
    DDE.set_integration_parameters(atol=at,rtol=rt)
    DDE.past_from_function([t_jit])
    DDE.step_on_discontinuities()
    print(DDE.t)
    data = []
    t_j = np.linspace(DDE.t+0.01, tf+0.01, 101)
    dt_jit = []
    for ti in t_j:
        data.append(DDE.integrate(ti))
        dt_jit.append(DDE.dt)
    y_j = np.asarray(data).T[0,:]

    sol.append(sol23)
    sol45.append(sol4)
    sol_jit.append([t_j, y_j, dt_jit])
# t = sol23.t
import scipy.io as spio
mat15 = spio.loadmat('data_dde23/wright1_5_dde23.mat', squeeze_me=True)
mat2 = spio.loadmat('data_dde23/wright2_dde23.mat', squeeze_me=True)
mat25 = spio.loadmat('data_dde23/wright2_5_dde23.mat', squeeze_me=True)
mat3 = spio.loadmat('data_dde23/wright3_dde23.mat', squeeze_me=True)

f90 = np.loadtxt("data_dde_solver_fortran/wright.dat")
t_f90 = f90[:,0]
y_f90 = f90[:,1].T

plt.figure(figsize=(18,14))
for i, li in enumerate(l):
    if li > 2.9:
        print('RK23 relative err to ref %s' % np.abs(((l3ref-sol[i].y[0,-1])/l3ref)))
        print('RK45 relative err to ref %s' % np.abs(((l3ref-sol45[i].y[0,-1])/l3ref)))
        print('jicdde relative err to ref %s' % np.abs(((l3ref-sol_jit[i][1][-1])/l3ref)))
        plt.plot(sol_jit[i][0], sol_jit[i][1], 'o-', label=r'jit l = %s' % li)
        plt.plot(sol45[i].t, sol45[i].y[0,:], 'o-', label=r'RK45 l = %s' % li)
        plt.plot(sol[i].t, sol[i].y[0,:], 'o-', label=r'RK23 l = %s' % li)
    # plt.plot(t_mat, y_mat,'-', label=r'dde23 $\theta(t)$')
    plt.legend()
    # plt.savefig('figures/suitecase/t_y_yp')
# plt.plot(mat15['t'], mat15['y'], 'o',  label=r'dde23 l = 1.5')
# plt.plot(mat2['t'], mat2['y'], 'o', label=r'dde23 l = 2')
# plt.plot(mat25['t'], mat25['y'], 'o', label=r'dde23 l = 2.5')
plt.plot(t_f90, y_f90, 'o-', label=r'fortran')
plt.plot(mat3['t'], mat3['y'], 'o-', label=r'dde23 l = 3.')
plt.ylabel(r'$y$')
plt.xlabel('t')
plt.legend()
plt.savefig('figures/wright/y')

plt.figure(figsize=(18,14))
for i, li in enumerate(l):
    if li > 2.9:
        plt.plot(sol_jit[i][0], sol_jit[i][2], 'o-', label=r'dt jit l = %s' % li)
        plt.plot(sol45[i].t[:-1], np.diff(sol45[i].t), 'o-', label=r'dt RK45 l = %s' % li)
        plt.plot(sol[i].t[:-1], np.diff(sol[i].t), 'o-', label=r'dt RK23 l = %s' % li)
        plt.legend()
    # plt.savefig('figures/suitecase/t_y_yp')
# plt.plot(mat15['t'][:-1], np.diff(mat15['t']), 'o',  label=r'dt dde23 l = 1.5')
# plt.plot(mat2['t'][:-1], np.diff(mat2['t']), 'o', label=r'dt dde23 l = 2')
# plt.plot(mat25['t'][:-1], np.diff(mat25['t']), 'o', label=r'dt dde23 l = 2.5')
plt.plot(t_f90[:-1], np.diff(t_f90), 'o-', label=r'fortran')
plt.plot(mat3['t'][:-1], np.diff(mat3['t']), 'o', label=r'dt dde23 l = 3.')
plt.ylabel(r'$\Delta t$')
plt.xlabel('t')
plt.legend()
plt.savefig('figures/wright/dt')

plt.show()

