import time
from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import CubicHermiteSpline

"""
The suitcase problem (Example 8 from Shampine 2000, Solving Delay Differential 
Equations with dde23)

Tested features:

    - initial discontinuities
    - restart integration
    - events locations

Comparison with dde23 and the fortan routine DDE_SOLVER_M.
"""
t0 = 0.0
tf = 12
tspan = [t0, tf]
tau = .1
gamma = 0.248
beta  = 1
A = 0.75
omega = 1.37
eta = np.arcsin(gamma/A);
def h(t):
    return y0

atol = 1e-10
rtol = 1e-5
delays = [tau]

print("\nKind of Event:               solve_dde         dde23       reference    DDE_SOLVER")
# ref values of matlab dde23 example script 
ref = [4.516757065, 9.751053145, 11.670393497]
# computed values from matlab dde23 with same atol & rtol
mat = [4.5167708185, 9.7511043904, 11.6703836720]
# from DDE_SOLVER  fortran routine example : Example 4.4.5: Events and Change Routine
f90 = [4.5167570861630821, 9.7510847727976273, 11.670385883524640]

def fun(t,y,Z):
    y_tau = Z[:,0]
    return [y[1],
            np.sin(y[0]) - np.sign(y[0]) * gamma * np.cos(y[0]) - beta * y_tau[0]
            + A * np.sin(omega * t + eta)]

def finalEvent(t,y,Z):
    return np.abs(y[0])-np.pi*.5
finalEvent.direction = 0 # % All events have to be reported
finalEvent.terminal = True

def hitGround(t,y,Z):                                     
    return y[0]                            
hitGround.direction = 0 # % All events have to be reported
hitGround.terminal = True                                 

events = [finalEvent, hitGround]
m = ['RK23', 'RK45', 'RK56']
t_ev_m = []
for method in m:
    tspan = [t0, tf]
    y0 = [0.0, 0.0]
    print('===================\n%s' % method)
    sol = solve_dde(fun, tspan, delays, y0, y0, method=method,
                      atol=atol, rtol=rtol ,events=events, dense_output=True)
    
    e = 0
    while(sol.t[-1]<tf):
        if not (sol.t_events[0]): # if there is not finalEvent 
            print('A wheel hit the ground. ',sol.t[-1],'',mat[e],'',ref[e],'',f90[e])
            t_val = np.array([sol.t[-1],mat[e],ref[e],f90[e]])
            print('relative error to ref   ', np.abs(t_val-ref[e])/ref[e])
            y0 = [0.0, sol.y[1,-1]*0.913]
            tspan = [sol.t[-1],tf]
            sol = solve_dde(fun, tspan, delays, y0, sol, method=method,
                      atol=atol, rtol=rtol ,events=events, dense_output=True)
            e += 1
        else:
            print("The suitcase fell over. ",sol.t[-1],'',mat[e],'',ref[e],'',f90[e])
            t_val = np.array([sol.t[-1],mat[e],ref[e],f90[e]])
            print('relative error to ref   ', np.abs(t_val-ref[e])/ref[e])
            break
    
    t_ev =np.sort(np.concatenate((sol.t_events)))
    t_ev_m.append(t_ev)
    print('nfailed %s nfev %s overlapping%s' % (sol.nfailed, sol.nfev, sol.nOverlap))
    del sol
for i in range(len(m)):
    print('%s -> t_ev %s' %(m[i], t_ev_m[i]))
    print('err %s' % (np.abs((t_ev_m[i]-np.asarray(ref))/np.asarray(ref))))

