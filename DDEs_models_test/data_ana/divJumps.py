from sympy import *
import numpy as np


t= symbols("t")
y =  Function("y")
dydt = y(t).diff(t)

tMoS = np.arange(0.0, 5.2, 0.2)
yMoS = [t + 1, -t + 1.4, t + 0.6, -t + 1.8, t + 0.2]

for i in range(5, len(tMoS)):
    y_p = yMoS[i-(1+4)]
    # print(y_p)

    eq = Eq( dydt , y_p.subs(t,t-1))
    y_cont = yMoS[i-1]
    t_i = tMoS[i]
    # print('y_cont', y_cont, 't_i', t_i)
    ic = y_cont.subs(t, t_i)
    res = dsolve( eq, ics={y(t_i): ic } )
    yMoS.append(res.rhs)

for i in range(len(yMoS)):
    print('yMoS[i]', yMoS[i])
