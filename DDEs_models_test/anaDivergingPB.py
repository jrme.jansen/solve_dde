import numpy as np
import matplotlib.pyplot as plt
from scipy.special import factorial

def anaMoins(t, tau, mu, phi):
    """
    Solution of $y'(t>=0)=\mu y(t-tau) $ 
    et $y(t<=0)=h$
    $y(t) = \sum_0^{[t/\tau]+1} 
        \frac{(\mu(t-(n-1)\tau))^n}{n!}$
    t :float
        current time
    tau : float
        delay
    phi : float
        past state t<=t0
    """
    s = 0.
    print('t', t)
    for n in range(int(np.floor(t/tau)) + 2):
        print('n', n)
        s += (mu*(-t + (n - 1.) * tau)**n) / \
                factorial(n)
    return s * phi
def ana(t, tau, mu, phi):
    """
    Solution of $y'(t>=0)=\mu y(t-tau) $ 
    et $y(t<=0)=h$
    $y(t) = \sum_0^{[t/\tau]+1} 
        \frac{(\mu(t-(n-1)\tau))^n}{n!}$
    t :float
        current time
    tau : float
        delay
    phi : float
        past state t<=t0
    """
    s = 0.
    print('t', t)
    for n in range(int(np.floor(t/tau)) + 2):
        print('n', n)
        s += (mu*(t - (n - 1.) * tau)**n) / \
                factorial(n)
    return s * phi

tau = 1.0
t0 = 0.0
tf = 10.
t_ = np.linspace(t0,tf, 101)
phi = 1.0
mu = 1.0

ana_ = np.zeros(t_.shape)
ana_Moins = np.zeros(t_.shape)
for i in range(len(t_)):
    ana_[i] = ana(t_[i], tau, mu, phi)
    ana_Moins[i] = anaMoins(t_[i], tau, mu, phi)

plt.figure()
plt.plot(t_, ana_)
plt.plot(t_, ana_Moins)
plt.show()


