from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np

"""
The  Marriott problem (Example 7 from  Shampine 2000, Solving Delay Differential
Equations with dde23)

Tested features:
    - terminal events locations
    - changes in equations and restart
Comparison with dde23 and the fortan routine DDE_SOLVER_M.
"""

xb = -0.427
a = 0.16
eps = 0.02
u = 0.5
tau = 1.0

args = (xb, a, eps, u, tau)

def fun(t, y, Z, xb, a, eps, u, tau):
    delta = Z[:,0] - xb
    sign = np.sign(delta)
    # print('t', t, 'sign', sign)
    return [(-y[0] + np.pi * (a + eps * sign - u * np.sin(delta)**2))/tau]

def ev(t, y, Z, xb, a, eps, u, tau):
    delta = Z[:,0] - xb
    return delta
ev.terminal = True
ev.direction = 0

t0 = 0.0
tf = 120


delay = 12.0
delays = [delay]
y0 = [0.6]
atol = 1e-8
rtol = 1e-5
tspan = [t0, tf]

print('y0', y0)

sol = solve_dde(fun, tspan, delays, y0, y0, method='RK23',
                args=args, events=ev,
                atol=atol, rtol=rtol)
print('nfaild %s nfev %s ' % (sol.nfailed, sol.nfev))
while sol.t[-1] < tf:
    print(' restart the integration at ', sol.t[-1])
    args = (xb, a, eps, u, tau)
    # jumps = [sol.t[-1]]
    tspan = [sol.t[-1], tf]
    y0 = sol.y[:,-1]
    sol = solve_dde(fun, tspan, delays, y0, sol, method='RK23',
                    args=args, events=ev,
                    # jumps=jumps,
                    atol=atol, rtol=rtol)
    print('nfaild %s nfev %s ' % (sol.nfailed, sol.nfev))


t = sol.t
y = sol.y[0,:]
yp = sol.yp[0,:]

# sol matlab
import scipy.io as spio
path_matlab = 'data_dde23/marriott_dde23.mat'
mat = spio.loadmat(path_matlab, squeeze_me=True)

t_mat = mat['t']
y_mat = mat['y']
yp_mat = mat['yp']
y_ev_mat = mat['y_events']
t_ev_mat = mat['t_events']

print('err t_events solve_dde / dde23', (sol.t_events[0] - t_ev_mat) / t_ev_mat)

plt.figure()
plt.plot(t, y, label='solve_dde')
plt.plot(t_mat, y_mat, label='dde23')
plt.plot(sol.t_events[0], sol.y_events[0], 'o', label='events')
plt.plot(t_ev_mat, y_ev_mat, 'o', label='events mat')
plt.xlabel(r'$t$')
plt.xlabel(r'$y(t)$')
plt.legend()
plt.savefig('figures/marriott/y')

plt.figure()
plt.plot(t[:-1],np.diff(t),'-o',label='dt scipy-dev')
plt.plot(t_mat[:-1],np.diff(t_mat),'-o',label='dt matlab solver')
plt.legend()
plt.ylabel(r'$\Delta t$')
plt.xlabel(r'$t$')
plt.savefig('figures/marriott/dt')
plt.show()
