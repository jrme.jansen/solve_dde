from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np
from jitcdde import jitcdde
from jitcdde import y as y_jit
from jitcdde import t as t_jit

"""
Model from Buddrus-Schiemann et al. 2014
Example used for stiff DDEs in julia DDE presentation : 
Solving Delay Differential Equations with Julia, JuliaCon 2019, 23 July 2019.

Comparison between jitcdde and solve_dde
"""
def fun(t,y,Z,D,S0,gammaS,Km,ns,
        alphaA,betaA,n1,alphaC,Rc,KE,alphaL,n2,C2):
    y_tau = Z[:,0]
    C_tau = y_tau[3]
    S, N, A, C, L = y
    return [
    D*S0 - gammaS * N * ((S**ns)/(Km**ns+S**ns)) - D*S,
    a * N * ((S**ns)/(Km**ns+S**ns)) - D *N,
    (alphaA + betaA*((C**n1)/(C1**n1+C**n1))) * N - \
            gammaA * A - alphaC*(Rc-C)*A + gamma3 * C - D*A \
            -KE*A*L,
    alphaC*(Rc-C)*A - gamma3 * C,
    alphaL * ((C_tau**n2)/(C2**n2 + C_tau**n2)) * N -\
            gammaL * L - D * L]

tau = 2
S0 = 1.0; N0 = 8.4e8; A0=2.5e-9; C0 = 7.6e-8; L0=5e-15
y0 = [S0, N0, A0, C0, L0]
D = 0.1
a = 0.66
gammaS = 1.3e-12
Km = 0.38
ns = 1.3
alphaA = 2.3e-19
betaA = 2.3e-18
n1 = 2.3
C1 = 70e-9
gammaA = 0.05
alphaC = 4e4
gamma3 = 0.08
Rc = 5e-7
KE = 1.5e-4
alphaL = 1.1e-8
gammaL = 0.005
C2 = 70e-9
n2 = 2.5

args = (D,S0,gammaS,Km,ns,alphaA,betaA,n1,
        alphaC,Rc,KE,alphaL,n2,C2)
t0 = 0.0
tf = 40.0
atol = 1e-10
rtol = 1e-5
tspan = [t0, tf]
delays = [tau]

sol23 = solve_dde(fun, tspan, delays, y0, y0,
                args=args, method='RK23',
                atol=atol, rtol=rtol)

sol45 = solve_dde(fun, tspan, delays, y0, y0,
        args=args, method='RK45',
        atol=atol, rtol=rtol)

t = sol23.t
S23 = sol23.y[0,:]
Sp = sol23.yp[0,:]
A23 = sol23.y[2,:]
A23p = sol23.yp[2,:]

t45 = sol45.t
S45 = sol45.y[0,:]
Sp45 = sol45.yp[0,:]
A45 = sol45.y[2,:]
A45p = sol45.yp[2,:]

#jit cdde
S = y_jit(0)
N = y_jit(1)
A = y_jit(2)
C = y_jit(3)
C_tau = y_jit(3,t_jit-tau)
L = y_jit(4)
f = [D*S0 - gammaS * N * ((S**ns)/(Km**ns+S**ns)) - D*S,
        a * N * ((S**ns)/(Km**ns+S**ns)) - D *N,
        (alphaA + betaA*((C**n1)/(C1**n1+C**n1))) * N - \
                gammaA * A - alphaC*(Rc-C)*A + gamma3 * C - D*A \
                -KE*A*L,
        alphaC*(Rc-C)*A - gamma3 * C,
        alphaL * ((C_tau**n2)/(C2**n2 + C_tau**n2)) * N -\
                gammaL * L - D * L]
DDE = jitcdde(f)
DDE.set_integration_parameters(atol=atol,rtol=rtol)
DDE.constant_past(y0)
DDE.step_on_discontinuities()
print(DDE.t)
data = []
t_jit = np.linspace(DDE.t+0.01, tf+0.01, 101)
dt_jit = []
for ti in t_jit:
    data.append(DDE.integrate(ti))
    dt_jit.append(DDE.dt)

A_jit = np.asarray(data).T[2,:]

plt.figure()
plt.plot(t, A23, label='solve_dde A RK23')
plt.plot(t45, A45, label='solve_dde A RK45')
plt.plot(t_jit, A_jit, 'o', label='jit A(t)')
# plt.plot(t_mat, y_mat, 'o', label='matlab dde23')
plt.xlabel(r'$t$')
plt.xlabel(r'$A$')
plt.legend()

plt.figure()
plt.plot(t[:-1], np.diff(t), label='dt RK23')
plt.plot(t45[:-1], np.diff(t45), label='dt RK45')
plt.plot(t_jit, dt_jit, 'o', label='dt jit')
# plt.plot(t_mat, y_mat, 'o', label='matlab dde23')
plt.xlabel(r'$t$')
plt.xlabel(r'$A$')
plt.legend()
plt.show()
