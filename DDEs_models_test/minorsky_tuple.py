import time
from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import CubicHermiteSpline

"""
Example 4.1 of Oberlet 1981
"""
t0 = 0.0
tf = 10
tspan = [t0, tf]
alpha = .1
a1 = 10.0
a2 = 25.0
a3 = 100.0
b = 0.05

t_histo = np.linspace(t0 - alpha, t0, 101)
h1 = 2 * np.pi * np.cos(20. * np.pi * t_histo)
h1p = -40 * np.pi**2 * np.sin(20. * np.pi * t_histo)
h2 = 1./10. * np.sin(20. * np.pi * t_histo)
h2p = 2 * np.pi * np.cos(20. * np.pi * t_histo)

y_h = np.vstack((h1, h2))
yp_h = np.vstack((h1p, h2p))

h = (t_histo, y_h, yp_h)
y0 = [2*np.pi, 0.5]

atol = 1e-8
rtol = 1e-6
delays = [alpha]

def fun(t,y,Z, a1, a2, a3, b):
    u, v = y
    u_tau, v_tau = Z
    return [-a1 * u - a2 * u_tau - a3 * v + b * u_tau**3.0,
            u]

args = (a1, a2, a3, b)
t1 = time.time()
sol23 = solve_dde(fun, tspan, delays, y0, h, method='RK23', args=args,
                  atol=atol, rtol=rtol, dense_output=True)
t2 = time.time()
print('tCPU ', t2-t1)

t = sol23.t
dI = sol23.y[0,:]
I = sol23.y[1,:]

t_in = np.linspace(t[0], t[-1], 101)
y_in = sol23.sol(t_in)

dI_in = y_in[0,:]
I_in = y_in[1,:]


Iref = -0.5735841564
dIref = 1.119559210
print('tf = ', t[-1])
print('I(tf)', I[-1], 'ref', Iref, 'err', np.abs((I[-1] - Iref)/Iref))
print('dIdt(tf)', dI[-1], 'ref', dIref, 'err', np.abs((dI[-1] - dIref)/dIref))
# path = 'data_dde23/suitcase_dde23.mat'
# import scipy.io as spio
# mat = spio.loadmat(path, squeeze_me=True)
# t_mat = mat['t']
# y_mat = mat['y'][0,:]
# yp_mat = mat['y'][1,:]


plt.figure(figsize=(18,14))
plt.plot(t, I, label=r'solve_dde $I(t)$')
plt.plot(t, dI, label=r'solve_dde $\dot{I}(t)$')
plt.plot(t_in, dI_in, label='interp dI')
# plt.plot(t_mat, y_mat,'-', label=r'dde23 $\theta(t)$')
# plt.plot(t_mat, yp_mat,'-', label=r'dde23 $\dot{\theta}(t)$')
plt.legend()
# plt.savefig('figures/suitecase/t_y_yp')

plt.figure(figsize=(14,12))
plt.plot(I, dI, 'o-', label='solve_dde')
# plt.plot(y_mat, yp_mat,'o',markerfacecolor='none', label='dde23 from Matlab')
plt.xlabel(r'$I$', fontsize=20)
plt.ylabel(r'$\dot{I}$', fontsize=20)
plt.legend()
# plt.savefig('figures/suitecase/phase_diag')
plt.figure()
plt.plot(t[:-1],np.diff(t),'-o',label='dt scipy-dev')
# plt.plot(t_mat[:-1],np.diff(t_mat),'-o',label='dt matlab solver')
plt.legend()
plt.ylabel(r'$\Delta t$')
plt.xlabel(r'$t$')
# plt.savefig('figures/solConv/dt')
plt.show()
