import time
from solve_dde import solve_dde
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import CubicHermiteSpline
from jitcdde import jitcdde
from jitcdde import y as y_jit
from jitcdde import t as t_jit

import scipy.io as spio
path_matlab = 'data_dde23/mackeyGlass_dde23_veryLong.mat'
mat = spio.loadmat(path_matlab, squeeze_me=True)
tf_mat = mat['tf']
tCPU_mat = mat['tCPU']

# f90 efficiency benchmarks data
f90 = np.loadtxt('data_dde_solver_fortran/dataMackeyGlassEfficiency_f90.dat')
tf_f90 = f90[:,0]
tCPU_f90 = f90[:,1]
sort = np.argsort(tf_f90)
tf_f90 = tf_f90[sort]
tCPU_f90 = tCPU_f90[sort]

def fun(t,y,Z):
    y_tau = Z[:,0]
    return [ beta * y_tau[0] / (1 + y_tau[0]**n) - gamma*y[0] ]

t0 = 0.0
tau = 15.0

n = 10
beta = 0.25
gamma = 0.1

atol = 1e-10
rtol = 1e-5
delays = [tau]
y0 = [1.0]
def h(t):
    return [1.]

f = [ beta * y_jit(0,t_jit-tau) / (1 + y_jit(0,t_jit-tau)**n) - gamma*y_jit(0) ]

tCPUjit = []
tCPUsolve_dde = []

for tf_ in tf_mat:
    # jitcdde
    
    tspan = [t0, tf_]
    t1 = time.time()
    sol23 = solve_dde(fun, tspan, delays, y0, h, method='RK23', atol=atol, rtol=rtol)
    t2 = time.time()
    tCPUsolve_dde.append(t2-t1)
    print('tCPU solve_dde', t2-t1)

    del sol23

for tf_ in tf_f90:
    # jitcdde
    print('\ntf', tf_)
    DDE = jitcdde(f)
    DDE.set_integration_parameters( atol = atol, rtol = rtol)
    t1 = time.time()
    DDE.constant_past([1.0])
    DDE.step_on_discontinuities()
    # print(DDE.t)
    data = []
    t_jit = np.linspace(DDE.t+0.01, tf_+0.01, 101)
    for ti in t_jit:
        data.append(DDE.integrate(ti))
    t2 = time.time()
    tCPUjit.append(t2-t1)

    print('tCPU jit', t2-t1)
    del data, DDE
    

plt.figure()
plt.loglog(tf_mat, tCPUsolve_dde,'o-', label='solve_dde tCPU')
plt.loglog(tf_mat, tCPU_mat,'o-', label='mat tCPU')
plt.loglog(tf_f90, tCPUjit,'o-', label='jit tCPU')
plt.loglog(tf_f90, tCPU_f90,'o-', label='fortran 90')

# plt.plot(y_mat,yp_mat,label='dde23')
plt.xlabel('tf')
plt.ylabel('tCPU (s)')
plt.legend()
plt.savefig('figures/mackeyGlass/tCPU_verylong')

plt.show()
