from itertools import product
from numpy.testing import (assert_, assert_allclose,
                           assert_equal, assert_no_warnings, suppress_warnings)
import pytest
from pytest import raises as assert_raises
import numpy as np
from scipy.special import factorial

from scipy.integrate._dde.dde import solve_dde
from scipy.integrate._dde.common import ContinuousExt
# from scipy.integrate._dde.base import ConstantDenseOutput

def compute_error(y, y_true, rtol, atol):
    e = (y - y_true) / (atol + rtol * np.abs(y_true))
    return np.sqrt(np.sum(np.real(e * e.conj()), axis=0) / e.shape[0])


def fun_div(t, y, Z):
    y_tau0 = Z[:,0]
    return np.array([ y_tau0 ])

def ana_div(t):
    phi = 1.; tau = 1.; s = 0.; mu = 1.0
    for n in range(int(np.floor(t/tau)) + 2):
        s += (mu*(t - (n - 1.) * tau)**n) / \
                factorial(n)
    return s * phi

def fun_conv(t, y, Z):
    y_tau0 = Z[:,0]
    return np.array([ - y_tau0 ])

def ana_conv(t):
    phi = 1.; tau = 1.; s = 0.; mu = 1.0
    for n in range(int(np.floor(t/tau)) + 2):
        s += (mu*(-t + (n - 1.) * tau)**n) / \
                factorial(n)
    return s * phi

def fun_suitcase(t, y, Z, gamma, beta, A, omega, eta):
    y_tau = Z[:,0]
    return [y[1],
            np.sin(y[0]) - np.sign(y[0]) * gamma * np.cos(y[0]) - \
                    beta * y_tau[0] + A * np.sin(omega * t + eta)]

def suitcase_finalEvent(t,y,Z, gamma, beta, A, omega, eta):
    return np.abs(y[0]) - np.pi * .5
suitcase_finalEvent.direction = 0
suitcase_finalEvent.terminal = True

def suitcase_hitGround(t,y,Z, gamma, beta, A, omega, eta):
    return y[0]
suitcase_hitGround.direction = 0
suitcase_hitGround.terminal = True

def virus_fun(t,y,Z):
    y_tau1 = Z[:,0]
    y_tau10 = Z[:,1]
    return [-y[0] * y_tau1[1]  + y_tau10[1],
            y[0] * y_tau1[1] -  y[1],
            y[1] - y_tau10[1]]

def virus_zero_y0(t,y,Z):
    y_tau1 = Z[:,0]
    y_tau10 = Z[:,1]
    return -y[0] * y_tau1[1]  + y_tau10[1]
virus_zero_y0.direction = -1
virus_zero_y0.terminal = False

def virus_zero_y1(t,y,Z):
    y_tau1 = Z[:,0]
    return y[0] * y_tau1[1] -  y[1]
virus_zero_y1.direction = -1
virus_zero_y1.terminal = False

def virus_zero_y2(t,y,Z):
    y_tau10 = Z[:,1]
    return y[1] - y_tau10[1]
virus_zero_y2.direction = -1
virus_zero_y2.terminal = False

def hopp_fun(t,y,Z, c, r, mu):
    if t <= 1 - c:
        f = -r * y[0] * 0.4 * (1 - t)
    elif t <= 1:
        f = -r * y[0] * (0.4 * (1 - t) + 10.0 - np.exp(mu) * y[0])
    elif t <= 2 - c:
        f = -r * y[0] * (10. - np.exp(mu) * y[0])
    else:
        f = -r * np.exp(mu) * y[0] * (Z[:,0] - y[0])
    return [f]


def test_conv_div():
    """ Features tested :
            * History function as callable, or constant
            * Terminal event locations
            * Restart with an initial discontinuity
    """
    re = 1e-3
    ae = 1e-6
    y0 = [1.0]
    def h_fun(t): return y0
    tau = 1.0
    delays = [tau]

    tspan = [0., 10.]

    for f in [[fun_conv, ana_conv], [fun_div, ana_div]]:
        for method, denseOutput, histo in product(['RK23', 'RK45', 'RK56'],
                [True, False], [y0, h_fun]):

            ref = f[1](tspan[-1])
            res = solve_dde(f[0], tspan, delays, y0, histo, rtol=re,
                            atol=ae, method=method, dense_output=denseOutput)

            assert_equal(res.t[0], tspan[0])
            assert_(res.t_events is None)
            assert_(res.y_events is None)
            assert_(res.success)
            assert_equal(res.status, 0)
            # ana arr
            ana_ = np.zeros(res.y[0,:].shape)
            for i in range(len(res.t)):
                ana_[i] = f[1](res.t[i])
            # mask values close to 0
            eps = 1e-2
            mask = np.abs(res.y[0,:]) > eps
            e = compute_error(res.y[0,mask], ana_[mask], re, ae)
            assert_(e < 1.)

            if res.sol is not None:
                # intepo test
                t_interp = np.linspace(*tspan)
                y_interp = res.sol(t_interp)

                assert_allclose(res.sol(res.t), res.y, rtol=1e-10, atol=1e-10)


def test_suitcase():
    """ Features tested :
            * History function as callable, or constant
            * Terminal event locations
            * Restart with an initial discontinuity
    """
    re = 1e-3
    ae = 1e-6
    y0 = [0.0, 0.0]
    def h_fun(t): return y0
    tau = .1
    delays = [tau]
    gamma = 0.248
    beta  = 1
    A = 0.75
    omega = 1.37
    eta = np.arcsin(gamma/A)
    args = (gamma, beta, A, omega, eta)
    t0 = 0.0; tf = 12.
    tspan = [t0, tf]
    # ref time where events occur
    ref = np.array([4.516757065, 9.751053145, 11.670393497])
    ev = [suitcase_finalEvent, suitcase_hitGround]
    for method, denseOutput, histo in product(['RK23', 'RK45', 'RK56'],[True, False],
            [y0, h_fun]):

        print('\nmethod', method,'initi', tspan, 'h', histo)
        res = solve_dde(fun_suitcase, tspan, delays, y0, histo, rtol=re,
                        atol=ae, args=args,  method=method, events=ev,
                        dense_output=denseOutput)
        while(res.t[-1]<tf):
            if not (res.t_events[0]):
                y0_ = [0.0, res.y[1,-1] * 0.913]
                tspan_ = [res.t[-1], tf]
                res = solve_dde(fun_suitcase, tspan_, delays, y0_, res, rtol=re,
                                atol=ae, args=args,  method=method, events=ev,
                                dense_output=denseOutput)
            else:
                # final event go out
                break

        assert_equal(res.t[0], t0)
        assert_(res.t_events is not None)
        assert_(res.y_events is not None)
        assert_(res.success)
        assert_equal(res.status, 0)

        t_events = np.sort(np.concatenate((res.t_events)))
        e = compute_error(t_events, ref, re, ae)
        assert_(np.all(e < 2))

        if res.sol is not None:
            # intepo test
            t_interp = np.linspace(res.t[0], res.t[-1])

            y_interp = res.sol(t_interp)
            assert_allclose(res.sol(res.t), res.y, rtol=1e-10, atol=1e-10)

def test_hoppensteadt():
    r = 0.5           
    mu = r / 10.0     
    c = np.sqrt(2)**-1

    tau = 1.0                      
    y0 = [10.0]                    
    def h_fun(t): return y0
    jumps = [1.0 - c, 1.0, 2.0 - c]
    t0 = 0.0                       
    tf = 10.0                      
    atol = 1e-8
    rtol = 1e-5                    
    tspan = [t0, tf]               
    delays = [tau]                 
    ref = 0.06302089869
    args = (c, r, mu)
    for method, denseOutput, histo in product(['RK23', 'RK45', 'RK56'],[True, False],
            [y0, h_fun]):
        res = solve_dde(hopp_fun, tspan, delays, y0, histo, method=method,
                 dense_output=denseOutput, atol=atol, rtol=rtol, args=args)
        assert_equal(res.t[0], t0)
        assert_(res.t_events is None)
        assert_(res.y_events is None)
        assert_(res.success)
        assert_equal(res.status, 0)

        e = np.abs((ref - res.y[0,-1]) / ref) < 10 * rtol
        assert_(e)

        if res.sol is not None:
            # intepo test
            t_interp = np.linspace(res.t[0], res.t[-1])

            y_interp = res.sol(t_interp)
            assert_allclose(res.sol(res.t), res.y, rtol=1e-10, atol=1e-10)

    
def test_virus():
    rtol = 1e-3
    atol = 1e-6
    t0 = 0.0; tf = 40
    tspan = [t0, tf]
    tau1 = 1.; tau10 = 10.
    delays = [tau1,tau10]

    y0 = [5.,.1,1.0]
    def h_fun(t): return y0
    ev = [virus_zero_y0, virus_zero_y1, virus_zero_y2]
    for method, denseOutput, histo in product(['RK23', 'RK45', 'RK56'],[True, False],
            [y0, h_fun]):

        print('\nmethod', method,'initi', tspan, 'h', histo)
        res = solve_dde(virus_fun, tspan, delays, y0, histo, method=method,
                events=ev, dense_output=denseOutput, atol=atol, rtol=rtol)

        assert_equal(res.t[0], t0)
        assert_(res.t_events is not None)
        assert_(res.y_events is not None)
        assert_(res.success)
        assert_equal(res.status, 0)

        # t_events = np.sort(np.concatenate((res.t_events)))
        # e = compute_error(t_events, ref, re, ae)
        # assert_(np.all(e < 5))

        if res.sol is not None:
            # intepo test
            t_interp = np.linspace(res.t[0], res.t[-1])

            y_interp = res.sol(t_interp)
            assert_allclose(res.sol(res.t), res.y, rtol=1e-10, atol=1e-10)

# def test_t_eval():
    # rtol = 1e-3
    # atol = 1e-6
    # y0 = [1/3, 2/9]
    # for t_span in ([5, 9], [5, 1]):
        # t_eval = np.linspace(t_span[0], t_span[1], 10)
        # res = solve_ivp(fun_rational, t_span, y0, rtol=rtol, atol=atol,
                        # t_eval=t_eval)
        # assert_equal(res.t, t_eval)
        # assert_(res.t_events is None)
        # assert_(res.success)
        # assert_equal(res.status, 0)

        # y_true = sol_rational(res.t)
        # e = compute_error(res.y, y_true, rtol, atol)
        # assert_(np.all(e < 5))

    # t_eval = [5, 5.01, 7, 8, 8.01, 9]
    # res = solve_ivp(fun_rational, [5, 9], y0, rtol=rtol, atol=atol,
                    # t_eval=t_eval)
    # assert_equal(res.t, t_eval)
    # assert_(res.t_events is None)
    # assert_(res.success)
    # assert_equal(res.status, 0)

    # y_true = sol_rational(res.t)
    # e = compute_error(res.y, y_true, rtol, atol)
    # assert_(np.all(e < 5))

    # t_eval = [5, 4.99, 3, 1.5, 1.1, 1.01, 1]
    # res = solve_ivp(fun_rational, [5, 1], y0, rtol=rtol, atol=atol,
                    # t_eval=t_eval)
    # assert_equal(res.t, t_eval)
    # assert_(res.t_events is None)
    # assert_(res.success)
    # assert_equal(res.status, 0)

    # t_eval = [5.01, 7, 8, 8.01]
    # res = solve_ivp(fun_rational, [5, 9], y0, rtol=rtol, atol=atol,
                    # t_eval=t_eval)
    # assert_equal(res.t, t_eval)
    # assert_(res.t_events is None)
    # assert_(res.success)
    # assert_equal(res.status, 0)

    # y_true = sol_rational(res.t)
    # e = compute_error(res.y, y_true, rtol, atol)
    # assert_(np.all(e < 5))

    # t_eval = [4.99, 3, 1.5, 1.1, 1.01]
    # res = solve_ivp(fun_rational, [5, 1], y0, rtol=rtol, atol=atol,
                    # t_eval=t_eval)
    # assert_equal(res.t, t_eval)
    # assert_(res.t_events is None)
    # assert_(res.success)
    # assert_equal(res.status, 0)

    # t_eval = [4, 6]
    # assert_raises(ValueError, solve_ivp, fun_rational, [5, 9], y0,
                  # rtol=rtol, atol=atol, t_eval=t_eval)


# def test_t_eval_dense_output():
    # rtol = 1e-3
    # atol = 1e-6
    # y0 = [1/3, 2/9]
    # t_span = [5, 9]
    # t_eval = np.linspace(t_span[0], t_span[1], 10)
    # res = solve_ivp(fun_rational, t_span, y0, rtol=rtol, atol=atol,
                    # t_eval=t_eval)
    # res_d = solve_ivp(fun_rational, t_span, y0, rtol=rtol, atol=atol,
                      # t_eval=t_eval, dense_output=True)
    # assert_equal(res.t, t_eval)
    # assert_(res.t_events is None)
    # assert_(res.success)
    # assert_equal(res.status, 0)

    # assert_equal(res.t, res_d.t)
    # assert_equal(res.y, res_d.y)
    # assert_(res_d.t_events is None)
    # assert_(res_d.success)
    # assert_equal(res_d.status, 0)

    # # if t and y are equal only test values for one case
    # y_true = sol_rational(res.t)
    # e = compute_error(res.y, y_true, rtol, atol)
    # assert_(np.all(e < 5))


#def test_no_integration():
#    for method in ['RK23', 'RK45', 'DOP853', 'Radau', 'BDF', 'LSODA']:
#        sol = solve_ivp(lambda t, y: -y, [4, 4], [2, 3],
#                        method=method, dense_output=True)
#        assert_equal(sol.sol(4), [2, 3])
#        assert_equal(sol.sol([4, 5, 6]), [[2, 2, 2], [3, 3, 3]])
#
#
#def test_no_integration_class():
#    for method in [RK23, RK45, DOP853, Radau, BDF, LSODA]:
#        solver = method(lambda t, y: -y, 0.0, [10.0, 0.0], 0.0)
#        solver.step()
#        assert_equal(solver.status, 'finished')
#        sol = solver.dense_output()
#        assert_equal(sol(0.0), [10.0, 0.0])
#        assert_equal(sol([0, 1, 2]), [[10, 10, 10], [0, 0, 0]])
#
#        solver = method(lambda t, y: -y, 0.0, [], np.inf)
#        solver.step()
#        assert_equal(solver.status, 'finished')
#        sol = solver.dense_output()
#        assert_equal(sol(100.0), [])
#        assert_equal(sol([0, 1, 2]), np.empty((0, 3)))
#
#
#def test_empty():
#    def fun(t, y):
#        return np.zeros((0,))
#
#    y0 = np.zeros((0,))
#
#    for method in ['RK23', 'RK45', 'DOP853', 'Radau', 'BDF', 'LSODA']:
#        sol = assert_no_warnings(solve_ivp, fun, [0, 10], y0,
#                                 method=method, dense_output=True)
#        assert_equal(sol.sol(10), np.zeros((0,)))
#        assert_equal(sol.sol([1, 2, 3]), np.zeros((0, 3)))
#
#    for method in ['RK23', 'RK45', 'DOP853', 'Radau', 'BDF', 'LSODA']:
#        sol = assert_no_warnings(solve_ivp, fun, [0, np.inf], y0,
#                                 method=method, dense_output=True)
#        assert_equal(sol.sol(10), np.zeros((0,)))
#        assert_equal(sol.sol([1, 2, 3]), np.zeros((0, 3)))
#
#
#def test_ConstantDenseOutput():
#    sol = ConstantDenseOutput(0, 1, np.array([1, 2]))
#    assert_allclose(sol(1.5), [1, 2])
#    assert_allclose(sol([1, 1.5, 2]), [[1, 1, 1], [2, 2, 2]])
#
#    sol = ConstantDenseOutput(0, 1, np.array([]))
#    assert_allclose(sol(1.5), np.empty(0))
#    assert_allclose(sol([1, 1.5, 2]), np.empty((0, 3)))


#def test_classes():
#    y0 = [1 / 3, 2 / 9]
#    for cls in [RK23, RK45]:
#        solver = cls(fun_rational, 5, y0, np.inf)
#        assert_equal(solver.n, 2)
#        assert_equal(solver.status, 'running')
#        assert_equal(solver.t_bound, np.inf)
#        assert_equal(solver.direction, 1)
#        assert_equal(solver.t, 5)
#        assert_equal(solver.y, y0)
#        assert_(solver.step_size is None)
#        assert_equal(solver.nfev, 0)
#        assert_equal(solver.njev, 0)
#        assert_equal(solver.nlu, 0)
#
#        message = solver.step()
#        assert_equal(solver.status, 'running')
#        assert_equal(message, None)
#        assert_equal(solver.n, 2)
#        assert_equal(solver.t_bound, np.inf)
#        assert_equal(solver.direction, 1)
#        assert_(solver.t > 5)
#        assert_(not np.all(np.equal(solver.y, y0)))
#        assert_(solver.step_size > 0)
#        assert_(solver.nfev > 0)
#        assert_(solver.njev >= 0)
#        assert_(solver.nlu >= 0)
#        sol = solver.dense_output()
#        assert_allclose(sol(5), y0, rtol=1e-15, atol=0)

